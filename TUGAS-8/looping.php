<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

    <h1>Berlatih Looping PHP</h1>

    <?php 
    echo "<h3>Contoh 1</h3>";
    echo "<h5>Looping 1</h5>";

    for ($i = 2; $i <= 20; $i+=2) {
        echo $i . " - I Love PHP <br> "; 
    }

    echo "<h5>Looping 2</h5>";
    for ($a = 20; $a >= 1; $a-=2) {
        echo $a . " - I Love PHP <br>";
    }

    echo "<h3>Soal No 2 Looping Array Modulo </h3>";
    $number = [18, 45, 29, 61, 47, 34];
    echo "Array number : " ;
    print_r($number);
    
    echo "<br>";
    echo "Array sisa baginya adalah:  "; 
    echo "<br>";
    
    echo "<br>";

    foreach ($number as $a) {
        $rest[] = $a %= 6;
    }

    print_r($rest);
    echo "<br>";


    echo "<h3>contoh 3</h3>";
    $biodata = [
        ["Hamdan",26, "cibarusah"],
        ["kanta",26, "karangsambung"],
        ["haris",26, "cikarang"],
    ];

    foreach($biodata as $key => $value) {
        $item = array(
            'nama' => $value[0],
            'umur' => $value[1],
            'alamat' => $value[2],
        );

        print_r($item);
        echo "<br>";
    }


    echo "<h3>contoh 4<h3>";
    for ($j = 1; $j <= 5; $j++) {
        for($b = 1; $b <= $j; $b++) {
            echo "*";
        }
        echo "<br>";
    }

    
    ?>
    
</body>
</html>