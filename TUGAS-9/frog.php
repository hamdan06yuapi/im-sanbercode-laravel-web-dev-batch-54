<?php 

require_once ('animal.php');

class Frog extends animal {

    public $name;
    public $legs = 4;
    public $cold_blooded = "no";
    public $jump = "hop hop";

    public function __construct($string)
    {
        $this->name = $string;
    }
}

?>