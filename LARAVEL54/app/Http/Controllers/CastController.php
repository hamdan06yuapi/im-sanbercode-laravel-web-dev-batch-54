<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index()
    {   
        $cast = DB::table('cast')->get();
        // dd($cars);
        return view('kategori.tampil', ['cast' => $cast]);
    }

    public function create()
    {
        return view('kategori.tambah');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $validated = $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        return redirect('/kategori');
    }


    public function show($id)
    {
        $kategori = DB::table('cast')->where('id', $id)->first();

        return view('kategori.detail', ['kategori' => $kategori]);

    }

    public function edit($id)
    {
        $kategori = DB::table('cast')->where('id', $id)->first();

        return view('kategori.edit', ['kategori' => $kategori]);
    }

    public function update ($id, Request $request)
    {
        $validated = $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        DB::table('cast')
        ->where('id', $id)
        ->update([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        return redirect('/kategori');
    }

    public function destroy($id)
    {
        $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/kategori');
    }
}
