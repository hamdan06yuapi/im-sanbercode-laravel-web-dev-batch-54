<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'create']);

Route::get('/biodata', [AuthController::class, 'table'] );

Route::post('/welcome', [AuthController::class, 'welcome'] );

Route::get('/data-table', function() {
     return view('halaman.table');   
});

//create
//CRUD CAST
route::get('/kategori/create', [CastController::class, 'create']);

route::post('/kategori', [CastController::class, 'store'] );

//read
route::get('/kategori', [CastController::class, 'index']);
//detail cast
route::get('/kategori/{id}', [CastController::class, 'show']);

// u -> update
route::get('/kategori/{id}/edit', [CastController::class, 'edit']);

route::put('/kategori/{id}', [CastController::class, 'update']);

// D -> delete
route::delete('/kategori/{id}', [CastController::class, 'destroy']);
