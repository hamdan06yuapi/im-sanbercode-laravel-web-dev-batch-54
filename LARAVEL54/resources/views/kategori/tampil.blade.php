@extends('layout.master')

@section('judul')
    Halaman list cast 
@endsection

@section('content')
    <a href="/kategori/create" class="btn btn-primary btn-sm">Tambah</a>

    <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Umur</th>
            {{-- <th scope="col">Bio</th> --}}
            <th scope="col">Aksi</th>
          </tr>
        </thead>
        <tbody>

            @forelse ($cast as $key => $value)
            <tr>
                <td>{{$key + 1}} </td>
                <td>{{$value->nama}}</td>
                <td>{{$value->umur}}</td>
                {{-- <td>{{$value->bio}}</td> --}}
                <td>
                
                <form action="/kategori/{{$value->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/kategori/{{$value->id}}" class="btn btn-primary btn-sm">Detail</a>
                    <a href="/kategori/{{$value->id}}/edit" class="btn btn-secondary btn-sm">Edit</a>
                    <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                </form>
                </td>
            </tr>
            @empty
                <tr>
                    <td>Tidak ada Data</td>
                </tr>
            @endforelse
          
        </tbody>
      </table>



@endsection