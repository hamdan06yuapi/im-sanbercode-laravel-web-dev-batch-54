@extends('layout.master')

@section('judul')
    Halaman Tambah Cast
@endsection


@section('content')

<form action="/kategori/{{$kategori->id}}" method="POST">
@csrf
@method('put')
    <div class="form-group">
      <label for="nama">Nama</label>
    <input type="text" name="nama" class="form-control" id="nama" value="{{$kategori->nama}}">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label for="umur">Umur</label>
      <input type="text" name="umur" class="form-control" id="umur" value="{{$kategori->umur}}">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="bio">Bio</label>
        <textarea name="bio" class="form-control" id="" cols="30" rows="10">{{$kategori->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection