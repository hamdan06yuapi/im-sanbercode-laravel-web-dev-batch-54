@extends('layout.master')

@section('judul')
  Detail cast
@endsection

@section('content')
    <h1>{{$kategori->nama}}</h1>
    <p>{{$kategori->bio}}</p>

    <a href="/kategori" class="btn btn-warning btn-sm">Kembali</a>
@endsection