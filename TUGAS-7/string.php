<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>

    <h1>Berlatih string PHP</h1>
    <?php 
    echo "<h3>Soal 1</h3>";

    $first_sentence = "Hello PHP";
    echo "Kalimat Pertama : " . $first_sentence . "</br>";
    echo "panjang String : " . strlen($first_sentence) . "</br>";
    echo "jumlah kata :" . str_word_count($first_sentence) . "</br>";
    echo "<br>";

    $second_sentence = "i'm ready for the chelenges";
    echo "Kalimat Pertama : " . $second_sentence . "</br>";
    echo "jumlah string : " . strlen($second_sentence) . "</br>";
    echo "jumlah kata : " . str_word_count($second_sentence) . "</br>";
    echo "<br>";

    echo "<h3>Soal No 2</h3>";

    $string2 = "I Love PHP";
    echo "<label>String: </label> \"$string2\" <br>";
    echo "Kata Pertama : " . substr($string2, 0,1) . "</br>";
    echo "Kata Kedua : " . substr($string2, 2,5) . "</br>";
    echo "Kata ketiga : " . substr($string2, 7,9) . "</br>";
    echo "<br>";

    echo "<h3> Soal No 3 </h3>";
    $string3 = "PHP is old but";
    echo "Kalimat ketiga : " . $string3 . "</br>";
    echo "Ganti kalimat ketiga : " . str_replace("PHP is old but", " PHP is but awesome", $string3) . "</br>";
    
    ?>
    
</body>
</html>